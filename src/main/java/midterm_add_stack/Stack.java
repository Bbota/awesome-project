package midterm_add_stack;

public class Stack {
    private Node group;
    private int size = 0;

    public Node getGroup(){
            return group;
    }
    public int size(){
        return size;
    }

    public void push(int data){
            Node newNode = new Node();
            newNode.setData(data);
            if (size != 0) {
                newNode.setNext(group);
            }
            group = newNode;
            size++;
    }
         public int pop(){
            Node Group = group;
            group = group.getNext();
            size--;
            return Group.getData();
        }

        public boolean IsEmpty(){
            if (size == 0){
                return true;
            }
            else
                return false;
        }

}


