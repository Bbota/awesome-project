package midterm_add_stack;

public class Node {
        private Node next;
        private int data;
        public Node getNext() {
            return next;
        }
        public void setData(int data) {
            this.data = data;
        }
        public void setNext(Node next) {
            this.next = next;
        }
        public int getData() {
          return data;
        }
}


