package midke;

public class task1 {
    public static void main (String[] args) {
        System.out.println("yyzzza " + stringClean("yyzzza"));
        System.out.println("abbbcdd " + stringClean("abbbcdd"));
        System.out.println("Hello " + stringClean("Hello"));


    }
    public static String stringClean(String str) {
        if (str.length() < 2)
            return str;
        if (str.charAt(0)==str.charAt(1))
            return stringClean(str.substring(1));
        else
            return str.charAt(0) + stringClean(str.substring(1));
    }
}
