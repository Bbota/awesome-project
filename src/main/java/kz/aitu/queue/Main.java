package kz.aitu.queue;

public class Main {
    public static void main(String[] args) {
        Stack Stack = new Stack();
        Stack.push(1);
        Stack.push(2);
        Stack.push(3);

        System.out.print("printing stack ");
        Node current = Stack.top;
        while (current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();

        System.out.println("pop: " + Stack.pop());
        System.out.println("empty or not " + Stack.empty());
        System.out.println("size: " + Stack.size());


        System.out.println();
    }
}

