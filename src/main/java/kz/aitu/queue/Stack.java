package kz.aitu.queue;

public class Stack {
    Node top;

    public String pop(){
        Node popped;
        popped = top;
        top = top.next;
        return popped.data;
    }

    public void push(int data) {
        Node newNode = new Node(data);
        if (top == null) {
            top = newNode;
        } else {
            newNode.next = top;
            top = newNode;
        }
    }

    public boolean empty() {
        if (top == null) {
            return true;
        } else {
            return false;
        }
    }

    public int size() {
        Node current = top;
        int sum=0;
        if (top == null) {
            return 0;
        } else {
            while (current != null) {
                current = current.next;
                sum++;
            }
        }
        return sum;
    }

    public Object top() {
        if (top == null) {
            return 0;
        } else {
            return top.data;
        }
    }
}

